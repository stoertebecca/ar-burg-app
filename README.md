AR-Burg-App

Die AR-Burg ist eine Aughmented Reality App für Besucher der Burg Rötteln in Lörrach.
Erstellt von 
    Rebecca Guth und Luisa Seitz
Für den Kurs Robotik, 6.Semester an der DHBW Lörrach.

Diese App wurde in React-Native auf der Plattform EXPO entwickelt.
Um die Entwicklungsumgebung zu starten muss Node.js und EXPO-CLI installiert sein:
npm install --global expo-cli

Die App kann mit dem Befehl 
npm start gestartet werden

