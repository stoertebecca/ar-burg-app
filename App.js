// Screens to navigate
import StartScreen from './components/StartScreen';
import VideoPlayer from './components/VideoPlayer';
import QRCodeScanner from './components/QRCodeScanner';
import PlayerHall from './components/PlayerHall';
import PlayerKitchen from './components/PlayerKitchen';
import PlayerTower from './components/PlayerTower';
import Player1300 from './components/Player1300';
import Player1500 from './components/Player1500';
import Contact from './components/Contact';
import CastleTour from './components/CastleTour';

// Needed imports for navigation
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

const stackNavigator = createStackNavigator({
    home: {
      screen: StartScreen,
      navigationOptions: {
        headerShown: false,
        title: 'Start'
      }
    },
    player: {
      screen: VideoPlayer,
      navigationOptions: {
        title: 'Die Burg',
        headerBackTitle: 'Zurück',
      }
    },
    scanner: {
      screen: QRCodeScanner,
      navigationOptions: {
        title: 'QR-Code scannen',
        headerBackTitle: 'Zurück',
      }
    },
    kitchen: {
      screen: PlayerKitchen,
      navigationOptions: {
        title: 'Die Küche',
        headerBackTitle: 'Zurück',
      }
    },
    hall: {
      screen: PlayerHall,
      navigationOptions: {
        title: 'Der Rittersaal',
        headerBackTitle: 'Zurück',
      }
    },
    tower: {
      screen: PlayerTower,
      navigationOptions: {
        title: 'Der Torturm',
        headerBackTitle: 'Zurück',
      }
    },
    castle1300: {
      screen: Player1300,
      navigationOptions: {
        title: 'Die Burg Rötteln um 1300',
        headerBackTitle: 'Zurück',
      }
    },
    castle1500: {
      screen: Player1500,
      navigationOptions: {
        title: 'Die Burg Rötteln um 1500',
        headerBackTitle: 'Zurück',
      }
    },
    contact: {
      screen: Contact,
      navigationOptions: {
        headerShown: false,
        title: 'Kontakt'
      }
    },
    tour: {
      screen: CastleTour,
      navigationOptions: {
        title: 'Burgtour starten',
        headerBackTitle: 'Zurück',
      }
    },
});

// Contains all available destinations
const navigator = createAppContainer(stackNavigator);

// Return the navigator, when starting the App
export default navigator;