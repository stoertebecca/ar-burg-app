import React from 'react';
import {
    StyleSheet, View, Text
} from 'react-native';
import { Video, Audio} from 'expo-av';
import kitchen from '../media/kitchen.mov';

// This class plays the Video of the kitchen.
export default class PlayerKitchen extends React.Component{
    constructor(props){
        super(props);
    } 
    async componentDidMount() {
        await Audio.setAudioModeAsync({ 
            playsInSilentModeIOS: true,
            allowsRecordingIOS: false,                                                                                                                                                                                   
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_MIX_WITH_OTHERS,                                                                         
            shouldDuckAndroid: false,                                                                                                           
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
            playThroughEarpieceAndroid: true
        })
    }
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.introText}>Erleben Sie die Küche hautnah!</Text>
                <Video 
                    source={kitchen}
                    shouldPlay
                    useNativeControls
                    resizeMode='cover'
                    style={{ width: '90%', height: '80%' }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    introText: {
        fontSize: 20,
        margin: 15,
        alignSelf: 'center'
    },
});