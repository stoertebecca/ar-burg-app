import React from 'react';
import {
    StyleSheet, View, Text, Image, Button, ScrollView
} from 'react-native';
import logo from '../assets/logo.png';

/*  This class shows the first screen the users sees.
    The user can start the tour or check the contact information.
*/
export default class StartScreen extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={styles.container}>
                <ScrollView>
                    <Image source={logo} alt='Logo' style={styles.image}/>
                    <Text style={styles.text}> Erlebe die </Text>
                    <Text style={styles.textBig}>Burg Rötteln</Text>
                    <Text style={styles.text}> wie noch nie zuvor! {'\n'}</Text>
                    <Text>{'\n'}</Text>
                    <Button 
                        title='Burgtour starten >' 
                        style={styles.button}
                        color='rgb(206, 95, 61)'
                        onPress={() => this.props.navigation.navigate('tour')} 
                    />
                    <Text style={styles.text}> </Text>
                    <Button 
                        title='Kontakt >' 
                        color='rgb(206, 95, 61)'
                        onPress={() => this.props.navigation.navigate('contact')} 
                        style={styles.button}
                    />
                    <Text></Text>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    image: {
        margin: 20,
        marginTop: 60,
        alignSelf: 'center',
    },
    text:{
        margin: 20,
        fontSize: 20,
        textAlign: 'center',
        color: 'rgb(110, 65, 35)'
    },
    textBig:{
        fontSize: 25,
        textAlign: 'center',
        color: 'rgb(206, 95, 61)'
    },
    textSmall:{
        margin: 15,
        fontSize: 16,
        textAlign: 'center',
        color: 'rgb(110, 65, 35)'
    },
    button:{
        margin: 10,
        alignSelf: 'center',
    },
});