import React from 'react';
import {
    StyleSheet, View, Text
} from 'react-native';
import { Video, Audio } from 'expo-av';
import castle1300 from '../media/roetteln_1300_final.mov';

// This class plays the Video of the castle from 1300.
export default class PlayerTower extends React.Component{
    constructor(props){
        super(props);
    }
    async componentDidMount() {
        await Audio.setAudioModeAsync({ 
            playsInSilentModeIOS: true,
            allowsRecordingIOS: false,                                                                                                                                                                                   
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_MIX_WITH_OTHERS,                                                                         
            shouldDuckAndroid: false,                                                                                                           
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
            playThroughEarpieceAndroid: true
        })
    }
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.introText}>Erleben Sie die Burg Rötteln um 1300 hautnah!</Text>
                <Video 
                    source={castle1300}
                    shouldPlay
                    useNativeControls
                    resizeMode='cover'
                    style={{ width: '90%', height: '80%' }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    introText: {
        fontSize: 20,
        margin: 15,
        alignSelf: 'center'
    },
});