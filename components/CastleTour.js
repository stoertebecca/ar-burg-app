import React from 'react';
import {
    StyleSheet, View, Text, Image, Button, ScrollView
} from 'react-native';
import logo from '../assets/logo.png';
import { MaterialCommunityIcons } from '@expo/vector-icons'

/*  This class is used to start the castle tour.
    The user can choose between an interactive or manual tour.
    For the interactive castle tour, the user can scan different QR-Codes.
    For the manual tour, the user just switches to the next screen.
*/
export default class CastleTour extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={styles.container}>
                <Image source={logo} alt='Logo' style={styles.image}/>
                <ScrollView>
                    <Text style={styles.text}>Interaktive Burgtour starten:</Text>
                    <MaterialCommunityIcons 
                        name='qrcode-scan'
                        style={styles.button}
                        size={150}
                        onPress = { () => this.props.navigation.navigate('scanner')}
                    /> 
                    <Text style={styles.text}>QR-Codes innerhalb der Burg scannen</Text>
                    <Text style={styles.text}>___________________________________</Text>
                    <Button 
                        title='Manuelle Burgtour starten >' 
                        style={styles.button}
                        color='rgb(206, 95, 61)'
                        onPress={() => this.props.navigation.navigate('player')} 
                    />
                    <Text></Text>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    image: {
        margin: 10,
        height: '20%',
        width: '50%',
        alignSelf: 'center',
    },
    text:{
        margin: 20,
        fontSize: 20,
        textAlign: 'center',
        color: 'rgb(110, 65, 35)'
    },
    textBig:{
        fontSize: 25,
        textAlign: 'center',
        color: 'rgb(206, 95, 61)'
    },
    textSmall:{
        margin: 15,
        fontSize: 16,
        textAlign: 'center',
        color: 'rgb(110, 65, 35)'
    },
    button:{
        margin: 20,
        alignSelf: 'center',
    },
});