import React from 'react';
import {
    StyleSheet, View, Text, ScrollView, Button, Image, TouchableOpacity
} from 'react-native';
import { Card } from 'react-native-elements';

import burg1300 from '../assets/burg1300.png';
import burg1500 from '../assets/burg1500.png';
import kueche from '../assets/kueche.png';
import saal from '../assets/saal.png';
import turm from '../assets/turm.png';

/*  This class is used to play the videos manually, 
    by clicking on the text or picture. 
    The pictures are a preview of the actual video, 
    so the user knows what he is about to watch.
*/
export default class VideoPlayer extends React.Component{
    constructor(props){
        super(props);
        this.state = {
//            url: castle1300, //this.props.navigation.state.params.object,
        };
    }
    render(){
        return(
            <View style={styles.container}>
                <ScrollView style={styles.scroll}>
                    <Card title='Gesamtansicht der Burg' titleStyle={styles.textBig}>
                        <Card>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('castle1300', {video: ''})}>
                                <Text style={styles.textSmall}>Die Burg um 1300</Text>
                                <Image source={burg1300} alt='Burg1300' style={styles.image} />
                            </TouchableOpacity>
                        </Card>
                        <Card>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('castle1500', {video: ''})}>
                                <Text style={styles.textSmall}>Die Burg um 1500</Text>
                                <Image source={burg1500} alt='Burg1500' style={styles.image} />
                            </TouchableOpacity>
                        </Card>
                    </Card>

                    <Card title='Rekonstruktionen der Räume' titleStyle={styles.textBig}>
                        <Card>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('tower', {video: ''})}>
                                <Text style={styles.textSmall}>Der Burgturm</Text>
                                <Image source={turm} alt='Turm' style={styles.image} />
                            </TouchableOpacity>
                        </Card>
                        <Card>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('kitchen', {video: ''})}>
                                <Text style={styles.textSmall}>Die Küche</Text>
                                <Image source={kueche} alt='Küche' style={styles.image} />
                            </TouchableOpacity>
                        </Card>
                        <Card>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('hall', {video: ''})}>
                                <Text style={styles.textSmall}>Der Rittersaal</Text>
                                <Image source={saal} alt='Saal' style={styles.image} />
                            </TouchableOpacity>
                        </Card>
                    </Card>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
    },
    text: {
        fontSize: 20,
        alignSelf: 'center',
    },
    textSmall:{
        margin: 15,
        fontSize: 20,
        textAlign: 'center',
        color: 'rgb(110, 65, 35)'
    },
    textBig:{
        fontSize: 25,
        textAlign: 'center',
        color: 'rgb(206, 95, 61)'
    },
    image: {
        alignSelf: 'center',
        height: 300,
        width: 300,
    },
    button: {
        alignSelf: 'center',
    },
    textBig:{
        fontSize: 25,
        textAlign: 'center',
        color: 'rgb(206, 95, 61)',
        marginTop: 20
    },
    scroll:{
        maxWidth: '100%',
    }
});