import * as React from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import * as Permissions from 'expo-permissions';

import { BarCodeScanner } from 'expo-barcode-scanner';
import PlayerTower from './PlayerTower';
import PlayerHall from './PlayerHall';
import PlayerKitchen from './PlayerKitchen';
import Player1300 from './Player1300';
import Player1500 from './Player1500';

/*  This class is used to scan the QR-Codes, which are placed
    around the castle. 
*/
export default class QRCodeScanner extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false,
    object: '', // Stores which QR-Code has been scanned = Which video shall be played
  };

  async componentDidMount() {
    this.getPermissionsAsync();
    this.props.navigation.addListener('willFocus', () => this.setState({scanned: false}));
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  render() {
    const { hasCameraPermission, scanned, object } = this.state;
    // Check if camera permissions are granted
    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }

    /*
      Depending on which QR-Code has been scanned, 
      the requested Video will be played
    */
    return (
      <View
        style={styles.container}>
        {(scanned && this.state.object === 'castle1300') ?
          <Player1300 /> : <Text />
        }
        {(scanned && this.state.object === 'castle1500') ?
        <Player1500 /> : <Text />
      }
        {(scanned && this.state.object === 'kitchen') ?
          <PlayerKitchen /> : <Text />
        }
        {(scanned && this.state.object === 'hall') ?
          <PlayerHall /> : <Text />
        }
        {(scanned && this.state.object === 'tower') ?
          <PlayerTower /> : <Text />
        } 
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
          style={styles.scanner}
        />

        {scanned && (
          <Button
            title={'QR-Code scannen'}
            onPress={() => this.setState({ scanned: false, object:'' })}
          />
        )}
        
      </View>
    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true, object: data });
    console.log(`Bar code with type ${type} and data ${data} has been scanned!`);
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  scanner: {
    width: '90%',
    height: '40%' ,
    margin: 5,
    alignSelf: 'center'
  }
})
