import React from 'react';
import {
    StyleSheet, View, Text, Image, Button, ScrollView
} from 'react-native';
import logo from '../assets/logo.png';

// This class shows the contact information.
export default class Contact extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={styles.container}>
                <ScrollView>
                    <Image source={logo} alt='Logo' style={styles.image}/>
                    <Text style={styles.textBig}>Kontakt</Text>
                    <Text style={styles.text}>
                        Hast du Fragen oder Anmerkungen?{'\n'}
                        Dann melde dich bei uns!
                    </Text>
                    <Text style={styles.text}>
                        DHBW Lörrach {'\n'}
                        Hangstraße 2 {'\n'}
                        79539 Lörrach {'\n'}
                        guthre@dhbw-loerrach.de {'\n'} seitzl@dhbw-loerrach.de
                    </Text>
                    <Text style={styles.text}>___________________________________</Text>
                    <Text style={styles.text}>Deine Ansprechpartner: </Text>
                    <Text style={styles.textSmall}>
                        WWI17B-SE {'\n'}
                        Rebecca Guth und Luisa Seitz
                    </Text>
                    <Button 
                        title='Zurück zur Startseite >' 
                        color='rgb(206, 95, 61)'
                        onPress={() => this.props.navigation.navigate('home')} 
                        style={styles.button} 
                    />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    image: {
        margin: 20,
        marginTop: 60,
        alignSelf: 'center',
    },
    text:{
        margin: 20,
        fontSize: 20,
        textAlign: 'center',
        color: 'rgb(110, 65, 35)'
    },
    textBig:{
        fontSize: 25,
        textAlign: 'center',
        color: 'rgb(206, 95, 61)'
    },
    textSmall:{
        margin: 15,
        fontSize: 16,
        textAlign: 'center',
        color: 'rgb(110, 65, 35)'
    },
    button:{
        margin: 10,
        alignSelf: 'center',
        color: 'rgb(206, 95, 61)'
    },
});